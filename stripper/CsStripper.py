from .StripperBase import StripperBase
import re


class CsStripper(StripperBase):
    """Stripper for C#.
     This class will remove:
        - Comment.
        - Multiple blank chars.
        - Multiple lines contains only space chars.
        - region pragma.
    """
    def __init__(self, **kwargs):
        super().__init__('c#', ['.cs'], **kwargs)

    def process(self, text):
        """Perform stripping given string.
         :param text: source code text string to be processed.
         :type text: str
         :return: processed source code text.
         :rtype: str
        """
        def commentReplacer(match):
            s = match.group(0)
            return " " if s.startswith('/') else s
        commentPattern = re.compile(
            r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
            re.DOTALL | re.MULTILINE)

        def spaceReplacer(match):
            s = match.group(0)
            if len(s) > 1:
                multiplier = len(s) // 4 if len(s) % 4 == 0 else len(s) // 2
                sp = ' ' * self.tabStop * multiplier
            else:
                sp = ' '
            return s if (s.startswith('\'') or s.startswith('"')) else sp
        spacePattern = re.compile(
            r'[\t ]+|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
            re.MULTILINE)

        def emptyLineReplacer(match):
            s = match.group(0)
            return s if (s.startswith('\'') or s.startswith('"')) else '\n'
        emptyLinePattern = re.compile(
            r'(\s*\n)+|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
            re.MULTILINE)

        regionPattern = re.compile(
            r'(#region[^\n]*\n)|(#endregion[^\n]*\n)',
            re.MULTILINE)
        text = re.sub(commentPattern, commentReplacer, text)
        text = re.sub(emptyLinePattern, emptyLineReplacer, text)
        text = re.sub(spacePattern, spaceReplacer, text)
        text = re.sub(regionPattern, '', text)
        return self.prefix + '\n' + text
