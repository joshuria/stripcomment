from abc import ABC, abstractmethod


class StripperBase(ABC):
    """Base class of stripping operation of languages."""
    def __init__(self, name, extList, **kwargs):
        """Constructor.
         :param name: language name of this tripper.
         :param extList: list of file extensions to be processed. Example: ['.cpp'].
         :type name: str
         :type extList: list[str]
        """
        self._name = name
        self._extList = extList
        self._settings = kwargs
        if 'ts' in self._settings:
            self._tabStop = int(self._settings['ts'])
            self._settings['tabstop'] = self._tabStop
            del self._settings['ts']
        elif 'tabstop' in self._settings:
            self._tabStop = int(self._settings['tabstop'])
        else:
            self._tabStop = 2
        self._prefix = ''
        if 'prefix' in self._settings:
            self._prefix = self._settings['prefix']

    @property
    def name(self):
        """Get programming language name."""
        return self._name

    @property
    def extList(self):
        """Get supported file extension list."""
        return self._extList

    @property
    def tabStop(self):
        """Get tab stop (# of space per tab)."""
        return self._tabStop

    @property
    def prefix(self):
        """Get additional prefix to be added to processed source code text."""
        return self._prefix

    @abstractmethod
    def process(self, text):
        """Perform stripping given string.
         :param text: source code text string to be processed.
         :type text: str
         :return: processed source code text.
         :rtype: str
        """
        raise NotImplemented('Abstract method')
