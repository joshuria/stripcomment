# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import os
import shutil
from stripper import *
from argparse import ArgumentParser


def dispatchFileToProcessor(filePath, outputPath):
    """Dispatch file to suitable processing function.
     :param filePath: path to file.
     :param outputPath: path to processed file saving location.
     :type filePath: str
     :type outputPath: str
    """
    cppExtension = ['.cpp', '.c', '.cxx', '.h', '.hpp', '.hxx', '.inc']
    csExtension = ['.cs']
    name, ext = os.path.splitext(filePath.lower())
    if ext in cppExtension:
        with open(filePath, 'r', encoding='utf-8-sig') as f:
            txt = f.read()
        sp = CppStripper(ts=args.tabsize, prefix=args.prefix)
        txt = sp.process(txt)
        # with open(filePath, 'w') as f:
        with open(outputPath, 'w') as f:
            f.write(txt)
    elif ext in csExtension:
        with open(filePath, 'r', encoding='utf-8-sig') as f:
            txt = f.read()
        sp = CsStripper(ts=args.tabsize, prefix=args.prefix)
        txt = sp.process(txt)
        # with open(filePath, 'w') as f:
        with open(outputPath, 'w') as f:
            f.write(txt)
    else:
        # Unsupport files: copy
        try:
            shutil.copyfile(filePath, outputPath)
        except shutil.SameFileError:
            # overwrite source file, skip here
            pass

def recursiveWalk(rootPath, outputPath):
    """Recursively visit files under given root path.
     :param rootPath: root path to folder contains files or path to file to be visited.
     :param outputPath: processed file will be saved to this directory.
     :type rootPath: str
     :type outputPath: str
    """
    if os.path.isfile(rootPath):
        outputPath = os.path.join(outputPath, os.path.basename(rootPath))
        dispatchFileToProcessor(rootPath, outputPath)
    else:
        for file in os.listdir(rootPath):
            # Skip .git .vs
            if file == '.git' or file == '.vs':
                continue
            srcPath = os.path.join(rootPath, file)
            outPath = os.path.join(outputPath, file)
            if os.path.isfile(srcPath):
                dispatchFileToProcessor(srcPath, outPath)
            elif os.path.isdir(srcPath):
                os.makedirs(outPath,  exist_ok=True)
                recursiveWalk(srcPath, outPath)


#######################################
#               Entry
#######################################
ArgParse = ArgumentParser(description='Source code stripper.')
ArgParse.add_argument(
    '-s', '--source',
    help='Path to source code root.')
ArgParse.add_argument(
    '-o', '--output',
    help='Output root path. Keep empty to overwrite source files.', default='')
ArgParse.add_argument(
    '-ts', '--tabsize',
    help='Tab size. Default is 4.', default=4)
ArgParse.add_argument(
    '-p', '--prefix',
    help='Prefix string to be prepended to processed code (extra new line will be added). Default is "".', default='')
args = ArgParse.parse_args()

if len(sys.argv) == 1:
    ArgParse.print_help()
    sys.exit(0)

if os.path.exists(args.source):
    os.makedirs(args.output, exist_ok=True)
    recursiveWalk(args.source, args.output)
else:
    print('Specified path: %s does not exist.' % args.source)
    sys.exit(-1)
