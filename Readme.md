# Strip Comments
Strip out all comments of source code files under given root path.

This project attempts to remove the following items in source code:
  * Comment.
  * Redundant space.
  * Empty lines.

# Require to Run
  - Python 3

# Supported Language
  - C/C++
